﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KlokExporter
{
    public partial class Guide : Form
    {
        public Guide()
        {
            InitializeComponent();
            richTextBox1.Rtf = @"{\rtf1\ansi When recording your time in Klok, make sure to use these formats (Always ALL CAPS):\line" +
                                @"   - \b WITS\b0 - Use just the WITS number.  e.g. 12345\line" +
                                @"   - \b WITS Code Review\b0 - Use the WITS number followed by REVIEW.  e.g. 12345 REVIEW\line" +
                                @"   - \b Help Desk\b0 - Use the state followed by HELPDESK.  e.g. NY HELPDESK\line" +
                                @"   - \b EPAD\b0 - Just use EPAD all caps  e.g. EPAD\line" +
                                @"   - \b 7 Habits\b0 - Just use 7HABITS all caps  e.g. 7HABITS\line" +
                                @"   - \b Company Meeting\b0 - Just use MEETING all caps  e.g. MEETING\line" +
                                @"   - \b Team Meeting\b0 - Just use CPMEETING all caps  e.g. CPMEETING\line" +
                                @"   - \b Enhancements\b0 - Use the enhancement code + ENHANCEMENT  e.g. WQM3 ENHANCEMENT\line" +
                                @"           (\b AR\b0 - SPE_AR_AUTOLINK_Constr)\line" +
                                @"           (\b BA\b0 - SPE_BA_ConfigMgr_Const)\line" +
                                @"           (\b BDS\b0 - SPE_BDS)\line" +
                                @"           (\b ABA\b0 - SPE_Convert_Abacus_Constr)\line" +
                                @"           (\b CSLA\b0 - SPE_CSLA_Reminders)\line" +
                                @"           (\b DGC\b0 - SPE_DGC_Improvement_Const)\line" +
                                @"           (\b FEIN1\b0 - SPE_FEINSSN_REDACT)\line" +
                                @"           (\b FEIN2\b0 - SPE_FEIN_ENCRYPTION_Const)\line" +
                                @"           (\b INSP1\b0 - SPE_Inspection_Conv_Const)\line" +
                                @"           (\b INSP2\b0 - SPE_Inspection_Conv_P2_Const)\line" +
                                @"           (\b MUSR\b0 - SPE_MUSR_API_Constr)\line" +
                                @"           (\b OAS1\b0 - SPE_OASDecommission_Phase1)\line" +
                                @"           (\b OAS2\b0 - SPE_OASDecommission_Phase2)\line" +
                                @"           (\b OAS3\b0 - SPE_OASDecommission_Phase3)\line" +
                                @"           (\b OAS4\b0 - SPE_OASDecommission_Phase4)\line" +
                                @"           (\b OAS5\b0 - SPE_OASDecommission_Phase5)\line" +
                                @"           (\b ORA\b0 - SPE_OracleAS_A&D_Project)\line" +
                                @"           (\b POC\b0 - SPE_POC_Reporting_Tool_Constr)\line" +
                                @"           (\b POL16\b0 - SPE_PolicyTxn16_Constr)\line" +
                                @"           (\b POL1\b0 - SPE_Policy_Imp_P1_Const)\line" +
                                @"           (\b POL2\b0 - SPE_Policy_Imp_P2_Const)\line" +
                                @"           (\b POL3\b0 - SPE_Policy_Imp_P3_DM_A&D)\line" +
                                @"           (\b POLQ1\b0 - SPE_Policy_Quality_P1_Constr)\line" +
                                @"           (\b POLQ2\b0 - SPE_Policy_Quality_P2_Constr)\line" +
                                @"           (\b USRDGC\b0 - SPE_USR_DGC_Constr)\line" +
                                @"           (\b WQM1\b0 - SPE_WQM_Imp_P1_Const)\line" +
                                @"           (\b WQM2\b0 - SPE_WQM_Imp_P2_Const)\line" +
                                @"           (\b WQM3\b0 - SPE_WQM_Imp_P3_Const)\line}";
        }
    }
}
