﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KlokExporter
{
    public partial class btnOpen : Form
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        public static string FILENAME = string.Empty;

        //clients
        const string CHANGE = "SP_MS_CHANGE";
        const string REPAIR = "SP_MS_REPAIR";
        const string CDXCHANGE = "CDX_MS_CHANGE";
        const string CDXREPAIR = "CDX_MS_REPAIR";
        const string HABIT7 = "7H Traing Found";
        const string FSI03 = "FSI03";

        //references
        const string ADMINHR = "(Admin) HR";
        const string EPAD = "EPaD Meetings";
        const string HELPDESK = "Help Desk";
        const string PNCGEN = "P&C - General";
        const string PNCOE = "P&C Operational Excellence";

        //tasks
        const string CP_SUPPORT = "CP_Support";
        const string CP_REVIEW = "CP_Review";
        const string AD_MEETING = "AD_Meetings";
        const string CP_MEETING = "CP_Meeting";
        const string CP_ENHANCE = "CP_Enhancements";
        const string CP_IMPROVE = "CP_Improvements";
        const string CP_PLANNING = "CP_Planning";
        const string CP_BUILD = "CP_BUILD";

        const string BILLABLE = "Billable";
        const string DONOTBILL = "Do Not Bill";

        public Dictionary<string, string> ENHANCEMENTS = new Dictionary<string, string>()
        {
            {"AR", "SPE_AR_AUTOLINK_Constr"},
            {"BA", "SPE_BA_ConfigMgr_Const"},
            {"BDS", "SPE_BDS"},
            {"ABA", "SPE_Convert_Abacus_Constr"},
            {"CSLA", "SPE_CSLA_Reminders"},
            {"DGC", "SPE_DGC_Improvement_Const"},
            {"FEIN1", "SPE_FEINSSN_REDACT"},
            {"FEIN2", "SPE_FEIN_ENCRYPTION_Const"},
            {"INSP1", "SPE_Inspection_Conv_Const"},
            {"INSP2", "SPE_Inspection_Conv_P2_Const"},
            {"MUSR", "SPE_MUSR_API_Constr"},
            {"OAS1", "SPE_OASDecommission_Phase1"},
            {"OAS2", "SPE_OASDecommission_Phase2"},
            {"OAS3", "SPE_OASDecommission_Phase3"},
            {"OAS4", "SPE_OASDecommission_Phase4"},
            {"OAS5", "SPE_OASDecommission_Phase5"},
            {"ORA", "SPE_OracleAS_A&D_Project"},
            {"POC", "SPE_POC_Reporting_Tool_Constr"},
            {"POL16", "SPE_PolicyTxn16_Constr"},
            {"POL1", "SPE_Policy_Imp_P1_Const"},
            {"POL2", "SPE_Policy_Imp_P2_Const"},
            {"POL3", "SPE_Policy_Imp_P3_DM_A&D"},
            {"POLQ1", "SPE_Policy_Quality_P1_Constr"},
            {"POLQ2", "SPE_Policy_Quality_P2_Constr"},
            {"USRDGC", "SPE_USR_DGC_Constr"},
            {"WQM1", "SPE_WQM_Imp_P1_Const"},
            {"WQM2", "SPE_WQM_Imp_P2_Const"},
            {"WQM3", "SPE_WQM_Imp_P3_Const"}
        };

        public Dictionary<string, string> CDXCOMPONENTS = new Dictionary<string, string>()
        {
            { "CDX Administration", "CDX Admin" },
            { "BEEP", "BEEP" },
            { "PEEP", "PEEP" },
            { "EXR", "EXR" },
            { "ETR", "ETR" },
            { "File Handler", "File Handler" },
            { "PEEP ASP.Net", "PEEP ASP" },
            { "SQL DB and OS Upgrade", "Upgrade" },
            { "SSO", "SSO" },
        };

        public Dictionary<string, string> REFERENCES = new Dictionary<string, string>()
        {
            {"ABACUS", "ABACUS"},
            {"ASSIGNED RISK", "Assigned Risk"},
            {"BATCH", "Batch"},
            {"BEEP", "BEEP"},
            {"CANC/REIN", "Canc/Rein"},
            {"CARRIER", "Carrier"},
            {"CLASS", "Class"},
            {"CORRESPONDENCE", "Correspondence"},
            {"CPAP-MN", "CPAP"},
            {"CPAP-OTHERS", "CPAP"},
            {"CRIT FINING", "Crit. Fining"},
            {"DGC", "DGC"},
            {"ELIGIBILITY", "Eligibility"},
            {"EXR", "EXR"},
            {"HC FEES MANAGEMENT", "HC Fees Mgmt"},
            {"ICR", "ICR"},
            {"IMAGING", "Imaging"},
            {"INSPECTION", "Inspection"},
            {"LETTERS", "Letters"},
            {"LINK DOCUMENTS", "Link Documents"},
            {"MANAGE POLICY", "Manage Policy"},
            {"MANAGE USR", "Manage USR"},
            {"MERIT", "Merit"},
            {"NOTES", "Notes"},
            {"ORDER", "Order"},
            {"OWNERSHIP", "Ownership"},
            {"PEEP", "PEEP"},
            {"POLICY", "Policy"},
            {"POLICY TAPE", "Policy Tape"},
            {"REMINDER", "Reminder"},
            {"REPORT", "Report"},
            {"SAFETY COMP", "Safety Comp"},
            {"SEARCH", "Search"},
            {"SYS LEVEL", "Sys Level"},
            {"TAKE OUT CREDIT", "Take out Credit"},
            {"TEST AUDIT", "Test Audit"},
            {"USR", "USR"},
            {"UST", "UST"},
            {"WCCRIT", "WCCRIT"},
            {"WCRATINGS", "WCRATINGS"},
            {"WQM", "WQM"}
        };

        public btnOpen()
        {
            InitializeComponent();
            string savedUser = Properties.Settings.Default.savedUser;
            if (!string.IsNullOrEmpty(savedUser))
                cmbNames.SelectedIndex = cmbNames.FindStringExact(savedUser);
        }

        private string getEnhancement(string enhancement)
        {
            string ret_val = string.Empty;

            if (ENHANCEMENTS.TryGetValue(enhancement, out ret_val))
                return ret_val;
            else
                return ret_val;
        }

        private string getReference(string reference)
        {
            string ret_val = string.Empty;

            if (REFERENCES.TryGetValue(reference, out ret_val))
                return ret_val;
            else
                return ret_val;
        }

        private string getCDXComponent(string component)
        {
            string ret_val = string.Empty;

            if (CDXCOMPONENTS.TryGetValue(component, out ret_val))
                return ret_val;
            else
                return ret_val;
        }

        private string getWITS(string wits)
        {
            string ret_val = string.Empty;

            ret_val = Regex.Match(wits, @"\d+").Value;

            return ret_val;
        }

        private int getTime(string time)
        {
            if (string.IsNullOrEmpty(time))
                return 0;

            int ret_val = 0;
            double minutes = TimeSpan.Parse(time).TotalMinutes;

            ret_val = ((int)Math.Round(minutes / 15.0)) * 15;

            double difference = minutes - ret_val;

            return ret_val;
        }

        private int GetQuarter(DateTime date)
        {
            if (date.Month >= 4 && date.Month <= 6)
                return 2;
            else if (date.Month >= 7 && date.Month <= 9)
                return 3;
            else if (date.Month >= 10 && date.Month <= 12)
                return 4;
            else
                return 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                FILENAME = openFileDialog.FileName;
                lblPath.Text = FILENAME;           
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FILENAME) || !Path.GetExtension(FILENAME).Equals(".csv"))
            {
                MessageBox.Show(this, "Must choose a csv file.");
                return;
            }
            string file = FILENAME;
            int placeholderCount = 0;

            List<Slip> slips = new List<Slip>();
            bool isFirst = true;
            string WITS = string.Empty;
            int time = 0;
            try
            {
                var totalLine = File.ReadLines(file).Last();
                string[] totalsArray = totalLine.Split(',');
                List<string> totals = new List<string>(totalsArray);
                List<DateTime> dates = new List<DateTime>();

                string[] lines = File.ReadAllLines(file);
                bool hitDescriptions = false;
                for (int l = 0; l < lines.Length; l++)
                {

                    var line = lines[l];
                    if (line == null)
                        continue;
                    var values = line.Split(',');

                    if (isFirst)
                    {
                        isFirst = false;
                        dates.Add(Convert.ToDateTime(values[4].Substring(4))); //Monday
                        dates.Add(Convert.ToDateTime(values[5].Substring(4))); //Tuesday
                        dates.Add(Convert.ToDateTime(values[6].Substring(4))); //Wednesday
                        dates.Add(Convert.ToDateTime(values[7].Substring(4))); //Thursday
                        dates.Add(Convert.ToDateTime(values[8].Substring(4))); //Friday
                        continue;
                    }

                    DateTime testDate;
                    if (string.IsNullOrEmpty(values[0]))
                    {
                        continue;
                    }
                    else if (DateTime.TryParse(values[0], out testDate))
                    {
                        hitDescriptions = true;
                    }

                    if (hitDescriptions)
                    {
                        DateTime testDate2;
                        DateTime compareDate = DateTime.Now;
                        for (int k = l; k < lines.Length; k++)
                        {
                            var line2 = lines[k];
                            if (line2 == null)
                                break;
                            var values2 = line2.Split(',');

                            if (string.IsNullOrEmpty(values2[0]))
                            {
                                continue;
                            }
                            else if (DateTime.TryParse(values2[0], out testDate2))
                            {
                                compareDate = testDate2;
                            }
                            else if (values2[0].Contains("(at"))
                            {
                                int s = values2[0].IndexOf("(");
                                string dProject = values2[0].Substring(0, s).Trim();
                                k++;
                                string descLine = lines[k];
                                string dDesc = descLine.Split(',')[0];

                                slips.Find(t => t.date == compareDate && t.project == dProject).description += " " + dDesc;
                            }
                        }

                        break;
                    }

                    for (int i = 0; i < 5; i++)
                    {
                        Slip slip = new Slip();
                        string consultant = string.Empty;
                        try
                        {
                            consultant = cmbNames.SelectedItem.ToString();
                        }
                        catch (NullReferenceException)
                        {
                            MessageBox.Show(this, "Please choose your name.");
                            return;
                        }
                        Properties.Settings.Default.savedUser = consultant;
                        Properties.Settings.Default.Save();

                        slip.consultant = consultant;
                        slip.billable = BILLABLE;
                        slip.description = "";

                        time = getTime(values[i + 4]);

                        if (time == 0)
                            continue;

                        slip.time = time;

                        slip.date = dates[i];

                        string project = values[0];

                        if (project.Equals("Totals"))
                            continue;

                        slip.project = project;

                        if (project.Contains("REVIEW"))
                            slip.task = CP_REVIEW;
                        else if (project.StartsWith("CDX"))
                        {
                            string number = project.Substring(3);
                            getCDXStuff(number, ref slip);
                            goto skip_wits;
                        }
                        else if (project.Equals("7HABITS"))
                        {
                            slip.task = AD_MEETING;
                            slip.client = HABIT7;
                            slip.reference = "";
                            slip.billable = DONOTBILL;
                            goto skip_wits;
                        }
                        else if (project.Equals("TRAINING"))
                        {
                            slip.task = CP_MEETING;
                            slip.client = FSI03;
                            slip.reference = PNCGEN;
                            slip.billable = DONOTBILL;
                            goto skip_wits;
                        }
                        else if (project.Contains("HELPDESK"))
                        {
                            string state = project.Substring(0, 2);
                            slip.task = CP_SUPPORT;
                            slip.client = "SP_" + state + "_SUPPORT";
                            slip.reference = HELPDESK;
                            slip.billable = BILLABLE;
                            goto skip_wits;
                        }
                        else if (project.Equals("MEETING"))
                        {
                            slip.task = AD_MEETING;
                            slip.client = FSI03;
                            slip.reference = ADMINHR;
                            slip.description = dates[i].ToString("MMMM") + " Company Meeting";
                            slip.billable = DONOTBILL;
                            goto skip_wits;
                        }
                        else if (project.Equals("PROCESS"))
                        {
                            slip.task = CP_IMPROVE;
                            slip.client = FSI03;
                            slip.reference = PNCOE;
                            slip.billable = DONOTBILL;
                            goto skip_wits;
                        }
                        else if (project.Equals("CPMEETING"))
                        {
                            slip.task = CP_MEETING;
                            slip.client = FSI03;
                            slip.reference = PNCGEN;
                            slip.billable = DONOTBILL;
                            goto skip_wits;
                        }
                        else if (project.Equals("EPAD"))
                        {
                            slip.task = AD_MEETING;
                            slip.client = FSI03;
                            slip.reference = EPAD;
                            int quarter = GetQuarter(dates[i]);
                            slip.description = "EPaD Q" + quarter.ToString() + " Meeting";
                            slip.billable = DONOTBILL;
                            goto skip_wits;
                        }
                        else if (project.Equals("PTO"))
                        {
                            slip.billable = DONOTBILL;
                            slip.client = "FSI10";
                            slip.task = "AD_PTO";
                            goto skip_wits;
                        }
                        else if (project.Contains("ENHANCEMENT"))
                        {
                            slip.task = CP_ENHANCE;
                            slip.client = getEnhancement(project.Replace("ENHANCEMENT", "").Trim());
                            slip.reference = "";
                            slip.billable = BILLABLE;
                            goto skip_wits;
                        }
                        else
                            slip.task = CP_SUPPORT;

                        WITS = getWITS(project);

                        int n;
                        if (int.TryParse(WITS, out n))
                        {
                            slip.extra = WITS;
                            slip.description = "WITS " + WITS.ToString();
                            if (slip.task.Equals(CP_REVIEW))
                                slip.description = "WITS " + WITS.ToString() + " Code Review";
                        }
                        else
                        {
                            slip.task = "TASK";
                            slip.client = "CLIENT";
                            slip.reference = "REFERENCE";
                            slip.billable = BILLABLE;
                            if (!string.IsNullOrEmpty(project))
                                slip.description = project;
                            placeholderCount++;
                            goto skip_wits;
                        }

                        using (WebClient wc = new WebClient())
                        {
                            wc.Credentials = new NetworkCredential("iis", "iis");

                            string Url = "https://www.iisprojects.com/wits/WitsIssue.aspx?IssueId=" + WITS;
                            string download = wc.DownloadString(Url);
                            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                            HtmlNode.ElementsFlags.Remove("option");
                            doc.LoadHtml(download);

                            string client = string.Empty;
                            string tempClient = doc.DocumentNode.SelectNodes("//select[@id='ctl00_ContentPlaceHolder1_ddlIssueType']//option[@selected]")[0].InnerText;

                            switch (tempClient)
                            {
                                case "CHANGE_REQ":
                                    client = CHANGE;
                                    break;
                                case "REPAIR":
                                    client = REPAIR;
                                    break;
                            }
                            slip.client = client;

                            string webRef = doc.DocumentNode.SelectNodes("//select[@id='ctl00_ContentPlaceHolder1_ddlSysVersion']//option[@selected]")[0].InnerText;
                            string reference = getReference(webRef);
                            slip.reference = reference;

                            HtmlAttributeCollection supportBudgetAttr = doc.DocumentNode.SelectNodes("//input[@id='ctl00_ContentPlaceHolder1_chkSupportBudget']")[0].Attributes;
                            if (supportBudgetAttr.Contains("checked"))
                                if(supportBudgetAttr["checked"].Value == "checked")
                                    slip.billable = BILLABLE;
                            else
                                    slip.billable = DONOTBILL;

                            HtmlAttributeCollection isReturnedAttr = doc.DocumentNode.SelectNodes("//input[@id='ctl00_ContentPlaceHolder1_chkIsReturned']")[0].Attributes;
                            if (isReturnedAttr.Contains("checked"))
                                if (isReturnedAttr["checked"].Value == "checked")
                                    slip.billable = DONOTBILL;

                            HtmlAttributeCollection isRegressionAttr = doc.DocumentNode.SelectNodes("//input[@id='ctl00_ContentPlaceHolder1_chkRegression']")[0].Attributes;
                            if (isRegressionAttr.Contains("checked"))
                                if (isRegressionAttr["checked"].Value == "checked")
                                    slip.billable = DONOTBILL;
                        }

                        skip_wits:

                        slips.Add(slip);
                    }
                }

                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "CSV|*.csv";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    var csv = new StringBuilder();
                    string filePath = save.FileName;
                    FILENAME = filePath;

                    var headers = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                            "Consultant", "Transaction Type", "Task", "Client", "Reference", "Extra", "Description",
                            "Start Date", "End Date", "Time Spent (HH:MM:SS)", "IsImported", "Billable");
                    csv.AppendLine(headers);

                    foreach (Slip s in slips)
                    {
                        string w_extra = s.extra;
                        TimeSpan span = TimeSpan.FromMinutes(s.time);
                        string w_time = span.ToString(@"hh\:mm\:ss");
                        DateTime w_date = s.date;
                        string w_reference = s.reference;
                        string w_consultant = s.consultant;
                        string w_task = s.task;
                        string w_client = s.client;
                        string w_description = s.description;
                        string w_billable = s.billable;

                        var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                            w_consultant, "Time", w_task, w_client, w_reference, w_extra, w_description,
                            w_date.ToString("MM/dd/yyyy"), w_date.ToString("MM/dd/yyyy"), w_time.ToString(), "N", w_billable);
                        csv.AppendLine(newLine);
                    }

                    File.WriteAllText(filePath, csv.ToString());
                }

                MessageBox.Show(this, string.Format("Your file has been successfully created. You have {0} slip(s) that needs more info.", placeholderCount.ToString()), "File Success", MessageBoxButtons.OK, MessageBoxIcon.None);

                System.Diagnostics.Process.Start(FILENAME);
            }
            catch (IOException)
            {
            }
        }

        static bool isCDXFirst = false;

        private void getCDXStuff(string number, ref Slip slip)
        {
            //login in stuff
            string url = "https://www.iisprojects.com/cdxsupport/show_bug.cgi";
            var email = "brian.soltis@farragut.com";
            var password = "cdxsupport";
            var restrictLogin = "on";
            string id = "992";
            string goAheadAndLogin = "Log in";
            string formParams = string.Format("Bugzilla_login={0}&Bugzilla_password={1}&Bugzilla_restrictlogin={2}&id={3}&GoAheadAndLogIn={4}", email, password, restrictLogin, id, goAheadAndLogin);
            string cookieHeader;
            WebRequest req = WebRequest.Create(url);
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(formParams);
            req.ContentLength = bytes.Length;
            using (Stream os = req.GetRequestStream())
            {
                os.Write(bytes, 0, bytes.Length);
            }
            WebResponse resp = req.GetResponse();
            cookieHeader = resp.Headers["Set-cookie"];

            string pageSource;
            string getUrl = "https://www.iisprojects.com/cdxsupport/show_bug.cgi?id=" + number;
            WebRequest getRequest = WebRequest.Create(getUrl);
            getRequest.Headers.Add("Cookie", cookieHeader);
            WebResponse getResponse = getRequest.GetResponse();
            using (StreamReader sr = new StreamReader(getResponse.GetResponseStream()))
            {
                pageSource = sr.ReadToEnd();
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(pageSource);

                slip.extra = number;
                slip.task = CP_SUPPORT;

                string client = string.Empty;
                string tempClient = doc.DocumentNode.SelectNodes("//select[@id='bug_severity']//option[@selected]")[0].InnerText;

                switch (tempClient)
                {
                    case "Enhancement":
                        client = CDXCHANGE;
                        break;
                    default:
                        client = CDXREPAIR;
                        break;
                }
                slip.client = client;

                string webRef = doc.DocumentNode.SelectNodes("//select[@id='component']//option[@selected]")[0].InnerText;
                string reference = getCDXComponent(webRef);
                slip.reference = reference;

                slip.billable = BILLABLE;
            }
            getResponse.Close();
            resp.Close();
            
        }

        public class WebClientEx : WebClient
        {
            public CookieContainer CookieContainer { get; private set; }

            public WebClientEx()
            {
                CookieContainer = new CookieContainer();
            }

            protected override WebRequest GetWebRequest(Uri address)
            {
                var request = base.GetWebRequest(address);
                if (request is HttpWebRequest)
                {
                    (request as HttpWebRequest).CookieContainer = CookieContainer;
                }
                return request;
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Guide g = new Guide();
            g.ShowDialog();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FILENAME = string.Empty;
            lblPath.Text = "File Path";
            cmbNames.SelectedIndex = -1;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOpen_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string filePath in files)
                {
                    FILENAME = filePath;
                    lblPath.Text = FILENAME;
                }
            }
        }

        private void btnOpen_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }
    }
}
