﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlokExporter
{
    class Slip
    {
        public string extra { get; set; }
        public int time { get; set; }
        public DateTime date { get; set; }
        public string reference { get; set; }
        public string consultant { get; set; }
        public string task { get; set; }
        public string client { get; set; }
        public string description { get; set; }
        public string billable { get; set; }
        public string project { get; set; }
    }
}
